<?php

use Phinx\Migration\AbstractMigration;

class Contatti extends AbstractMigration
{
    public function up()
    {
        // create the table
        $this->execute('CREATE TABLE contatti (
            idContatto INTEGER NOT NULL,
            Nome TEXT NOT NULL,
            Cognome TEXT NOT NULL,
            Email TEXT NOT NULL,
            NumeroDiTelefono INTEGER NOT NULL,
            Note TEXT NOT NULL,
            Privacy BOOLEAN NOT NULL,
        PRIMARY KEY (idContatto))');

        $this->execute('CREATE UNIQUE INDEX index_contatti ON contatti (idContatto)');

    }

    public function down()
    {
        $this->table('contatti')->drop();
    }
}
