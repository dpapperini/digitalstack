<?php
/**
 * Response class file
 *
 * PHP Version 7
 *
 * @category Response
 * @package  Digitalstack\dispatcher
 * @author   Daniele Papperini <daniele.papperini@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://smartimmo.com/
 */

namespace Digitalstack\dispatcher;

/**
 * Class Response
 *
 * @category Response
 * @package  Digitalstack\dispatcher
 * @author   Daniele Papperini <daniele.papperini@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://smartimmo.com/
 */
class Response
{
    /**
     * Body (For Now Not Use)
     *
     * @var
     */
    protected $body;

    /**
     * Headers
     *
     * @var
     */
    protected $headers;

    /**
     * Status Code
     *
     * @var
     */
    protected $statusCode;

    /**
     * Response constructor.
     *
     * @param $statusCode
     * @param $headers
     * @param $body
     */
    public function __construct($statusCode, $headers, $body)
    {
        $this->statusCode = $statusCode;
        $this->body = $body;
        $this->headers = $headers;
    }

    /**
     * Create Response
     *
     * @param null $statusCode
     * @param array $headers
     * @param null $body
     * @return Response
     */
    public static function create($statusCode = null, $headers = [], $body = null)
    {
        return new Response($statusCode, $headers, $body);
    }

    /**
     * Set Status Code
     *
     * @param $code
     * @return $this
     */
    public function setStatusCode($code)
    {
        $this->statusCode = $code;
        return $this;
    }

    /**
     * Set Header
     *
     * @param $header
     * @param $value
     * @return $this
     */
    public function setHeader($header, $value)
    {
        $this->headers[$header] = $value;
        return $this;
    }

    /**
     * Set Body (For Now Not Use)
     *
     * @param $body
     * @return $this
     */
    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    /**
     *  Send Header and Exit from the Script
     */
    public function send()
    {
        header('HTTP/1.1 '.$this->statusCode);
        foreach ($this->headers as $header => $value) {
            header(strtoupper($header).': '.$value);
        }
        exit;
    }
}