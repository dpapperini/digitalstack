<?php
/**
 * Dispatcher class file
 *
 * PHP Version 7
 *
 * @category Dispatcher
 * @package  Digitalstack\dispatcher
 * @author   Daniele Papperini <daniele.papperini@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://smartimmo.com/
 */

namespace Digitalstack\dispatcher;

use Exception;

/**
 * Class Dispatcher
 *
 * @category Dispatcher
 * @package  Digitalstack\dispatcher
 * @author   Daniele Papperini <daniele.papperini@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://smartimmo.com/
 */
class Dispatcher {

    /**
     * Params
     *
     * @var array
     */
    protected $params;

    /**
     * Controller
     *
     * @var
     */
    protected $controller;

    /**
     * Dispatcher constructor.
     *
     * @param $controller
     */
    public function __construct($controller) {
        $this->controller = $controller;
        $this->params = $this->parse($_SERVER['REQUEST_URI']);
    }

    /**
     *  Run My Controller
     */
    public function handle() {
        $action = $this->params['method'];
        $args = $this->params['args'];
        $query = $this->params['query'];

        try {

            if(method_exists($this->controller, $action)) {
                if(!is_null($args) && is_array($args)) {
                    call_user_func_array(array($this->controller, $action), $args);
                } else {
                    $this->controller->$action();
                }
            }

        } catch (Exception $e) {
            $e->getMessage();
        }
    }

    /**
     * Parse PATH
     *
     * For now I use it for match url and set variables and after call controller
     *
     * @param $path
     * @return array
     */
    protected function parse($path) {
        $parse_url = parse_url($path);
        $method = 'getError';
        $args = null;

        if (preg_match("#^/dettaglio/([0-9]+)/([0-9]+)$#", $path, $matches)) {
            $args[] = $matches[1];
            $args[] = $matches[2];
            $method = "getDettaglio";
        } else if (preg_match("#^/dettaglio/(edit|delete)/([0-9]+)$#", $path, $matches)) {
            $args[] = $matches[2];
            $method = $matches[1]."Dettaglio";
        } else if (preg_match("#^/(dettaglio|contatto)/add$#", $path, $matches)) {
            $method = "add".ucfirst($matches[1]);
        } else if (preg_match("#^/(login|logout)$#", $path, $matches)) {
            $method = $matches[1];
        } else if (preg_match("#^/(index)?$#", $path, $matches)) {
            $method = 'getIndex';
        }

        return [
            'method' => $method,
            'query' => isset($parse_url['query']) ? parse_str($parse_url['query']) : null,
            'args' => $args
        ];

    }

}
