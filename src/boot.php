<?php
/**
 * Boot class file
 *
 * PHP Version 7
 *
 * @category Boot
 * @package  Digitalstack
 * @author   Daniele Papperini <daniele.papperini@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://smartimmo.com/
 */

// Tell PHP that we're using UTF-8 strings until the end of the script
mb_internal_encoding('UTF-8');
$utf_set = ini_set('default_charset', 'utf-8');
if (!$utf_set) {
    throw new Exception('could not set default_charset to utf-8, please ensure it\'s set on your system!');
}

// Tell PHP that we'll be outputting UTF-8 to the browser
mb_http_output('UTF-8');

require_once __DIR__ . '/../vendor/autoload.php';

const VIEWS_FOLDER = __DIR__ . '/../views';

const SESSION_VIEWS_FOLDER = __DIR__ . '/../sessions';

const PATH_TO_SQLITE_FILE = __DIR__ . '/../data/smart.db';