<?php
/**
 * View class file
 *
 * PHP Version 7
 *
 * @category View
 * @package  Digitalstack
 * @author   Daniele Papperini <daniele.papperini@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://smartimmo.com/
 */

namespace Digitalstack;

use Exception;

/**
 * Class View
 *
 * @category View
 * @package  Digitalstack
 * @author   Daniele Papperini <daniele.papperini@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://smartimmo.com/
 */
class View
{

    /**
     * File Name
     *
     * @var string
     */
    protected $file;

    /**
     * Folder Name
     *
     * @var string
     */
    protected $folder;

    /**
     * Vars
     *
     * @var array
     */
    protected $variables;

    /**
     * View constructor.
     */
    function __construct()
    {
        $this->variables = [];
        $this->file = "index";
        $this->folder = "";
    }

    /**
     * Set the folder and filename for the view template file
     */
    public function setView($folder, $file)
    {
        $this->folder = $folder;
        $this->file = $file;
    }

    /**
     * Set the variables to be exported into the template file
     */
    public function setVariable($key, $value)
    {
        $this->variables[$key] = $value;
    }

    /**
     * Return img in base64
     *
     * @param $path
     * @return string
     */
    public static function getDataURI($path) {
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
        return $base64;
    }

    /**
     * Render the view file
     *
     * @throws Exception
     */
    public function render()
    {
        $filename = VIEWS_FOLDER . ($this->folder ? "/" . $this->folder : '') . "/" . $this->file . ".php";
        if (!file_exists($filename))
        {
            throw new Exception("View " . $filename . " doesn't exist.");
        }

        extract($this->variables); //Extract the variables into the page, so that each key is available inside the page as php variable
        // start buffering
        ob_start();
        include($filename);
        $output = ob_get_contents();
        // clean the buffer
        ob_end_clean();
        // end buffer
        ob_end_flush();
        // render page content
        echo $output;
    }

}
