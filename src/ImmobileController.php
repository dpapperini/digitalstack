<?php
/**
 * ImmobileController class file
 *
 * PHP Version 7
 *
 * @category Controller
 * @package  Digitalstack
 * @author   Daniele Papperini <daniele.papperini@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://smartimmo.com/
 */

namespace Digitalstack;

use Digitalstack\dispatcher\Response;
use Digitalstack\dispatcher\SecureSessionHandler;
use Digitalstack\model\Agenzie;
use Digitalstack\model\Annunci;
use Digitalstack\model\Contatti;

/**
 * Class ImmobileController
 *
 * @category Controller
 * @package  Digitalstack
 * @author   Daniele Papperini <daniele.papperini@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://smartimmo.com/
 */
class ImmobileController
{

    /**
     * View
     *
     * @var View
     */
    protected $view;

    /**
     * Response
     *
     * @var Response
     */
    protected $response;

    /**
     * Session
     *
     * @var SecureSessionHandler
     */
    public $session;

    /**
     * Message
     *
     * @var string
     */
    protected $msg;

    /**
     * ImmobileController constructor.
     */
    public function __construct()
    {
        $this->view = new View();
        $this->response = new Response(200, null, null);
        $this->session = new SecureSessionHandler('digitalstack', 'my_digitalstack');
        $this->msg = "";
    }

    /**
     * Set View File and Folder
     *
     * @param $folder
     * @param $file
     */
    protected function setView($folder, $file)
    {
        $this->view->setView($folder,$file);
    }

    /**
     * Set View Vars
     *
     * @param $key
     * @param $value
     */
    protected function setVariable($key, $value)
    {
        $this->view->setVariable($key, $value);
    }

    /**
     * Call View Render Method
     *
     * @throws \Exception
     */
    protected function render()
    {
        $this->view->render();
    }

    /**
     * Index Action
     *
     * @return void
     */
    public function getIndex()
    {
        $annunci_and_agenzie = Annunci::getAnnunciAndAgenzie();
        $agenzia = null;
        $agenzia_annunci = null;
        $isAuth = $this->isAuth();

        if (!$isAuth) {
            $agenzia = $this->session->get('session_agenzia');
            $agenzia_annunci = Annunci::getAll(['idAgenzia' => $agenzia->idAgenzia]);
        }

        $this->setView('', 'index');
        $this->setVariable('annunci', $annunci_and_agenzie);
        $this->setVariable('agenzia', $agenzia);
        $this->setVariable('agenzia_annunci', $agenzia_annunci);
        $this->setVariable('msg', $this->msg);
        $this->setVariable('isAuth', $isAuth);
    }

    /**
     * Get Dettaglio By idAgenzia and idImmobile
     *
     * @param $idImmobile idImmobile
     *
     * @return void
     */
    public function getDettaglio($idAgenzia, $idImmobile)
    {
        $errors = null;
        $isAuth = $this->isAuth();
        if (!is_null($idAgenzia) && !is_null($idImmobile)) {

            $agenzia = Agenzie::get(['idAgenzia' => $idAgenzia]);
            $annuncio = Annunci::get(['idAnnuncio' => $idImmobile]);

            if (!is_null($agenzia) && !is_null($annuncio)) {
                $contatto = new Contatti();
                $this->setView('', 'dettaglio');
                $this->setVariable('annuncio', $annuncio);
                $this->setVariable('agenzia', $agenzia);
                $this->setVariable('errors', $errors);
                $this->setVariable('msg', $this->msg);
                $this->setVariable('contatto', $contatto);
                $this->setVariable('isAuth', $isAuth);
            } else {
                $this->msg = "Nessun dettaglio trovato";
                $this->setVariable('msg', $this->msg);
                $this->setVariable('isAuth', $isAuth);
                self::getIndex();
                $this->setHeaders(['Location' => '/404'], 404);
            }

        } else {
            $this->msg = "Nessun dettaglio trovato";
            $this->setVariable('msg', $this->msg);
            $this->setVariable('isAuth', $isAuth);
            $this->setHeaders(['Location' => '/404'], 404);
        }
    }

    /**
     * isAuth
     *
     * Simple Check User Is Logged
     *
     * @return bool
     */
    private function isAuth()
    {
        return !$this->session->get('session_id') && !$this->session->get('session_agenzia');
    }

    /**
     * Set Headers in Response and Send
     *
     * @param array $headers
     * @param int $status
     */
    private function setHeaders($headers = [], $status = 200)
    {
        foreach ($headers as $key => $header) {
            $this->response->setHeader($key, $header);
        }
        $this->response->setStatusCode($status);
        $this->response->send();
    }

    /**
     * Edit Dettaglio by idImmobile and after check if user can modify it
     *
     * @param $idImmobile idImmobile
     *
     * @return void
     */
    public function editDettaglio($idImmobile)
    {
        $isAuth = $this->isAuth();
        if ($isAuth) {
            $this->setHeaders(['Location' => '/login'], 302);
        }

        $errors = null;
        $annuncio = new Annunci();
        if ($idImmobile) {
            $annuncio = Annunci::get(['idAnnuncio' => $idImmobile, 'idAgenzia' => $this->session->get('session_agenzia')->idAgenzia]);

            if (!$annuncio) {
                $this->msg = "Nessun dettaglio trovato";
                $this->setVariable('msg', $this->msg);
                $this->setVariable('isAuth', $isAuth);
                $this->setHeaders(['Location' => '/404'], 404);
            }

            if ($_SERVER['REQUEST_METHOD'] === "POST") {
                if (!array_key_exists('csrf_token', $_POST)) {
                    $this->msg = "Error csrf-token";
                } else if ($_POST['csrf_token'] !== $this->session->get('csrf_token')) {
                    $this->msg = "Invalid csrf-token";
                } else {
                    $Categoria = trim(filter_input(INPUT_POST, 'Categoria', FILTER_SANITIZE_STRING));
                    $Tipologia = trim(filter_input(INPUT_POST, 'Tipologia', FILTER_SANITIZE_STRING));
                    $Contratto = trim(filter_input(INPUT_POST, 'Contratto', FILTER_SANITIZE_STRING));
                    $Prezzo = trim(filter_input(INPUT_POST, 'Prezzo', FILTER_SANITIZE_NUMBER_INT));
                    $Comune = trim(filter_input(INPUT_POST, 'Comune', FILTER_SANITIZE_STRING));
                    $Superficie = trim(filter_input(INPUT_POST, 'Superficie', FILTER_SANITIZE_NUMBER_INT));
                    $NumeroLocali = trim(filter_input(INPUT_POST, 'NumeroLocali', FILTER_SANITIZE_NUMBER_INT));
                    $Immagine = trim(filter_input(INPUT_POST, 'Immagine', FILTER_SANITIZE_STRING));

                    $annuncio->Categoria = $Categoria;
                    $annuncio->Tipologia = $Tipologia;
                    $annuncio->Contratto = $Contratto;
                    $annuncio->Prezzo = $Prezzo;
                    $annuncio->Comune = $Comune;
                    $annuncio->Superficie = $Superficie;
                    $annuncio->NumeroLocali = $NumeroLocali;
                    $annuncio->Immagine = $Immagine;

                    if (!$this->validateFieldStringNotNullOrEmpty($Categoria)) {
                        $errors[] = "Inserire una categoria";
                    }

                    if (!$this->validateFieldStringNotNullOrEmpty($Tipologia)) {
                        $errors[] = "Inserire una tipologia";
                    }

                    if (!$this->validateFieldStringNotNullOrEmpty($Contratto)) {
                        $errors[] = "Inserire una contratto";
                    }

                    if (!$this->validateFieldIntNotNullOrZero($Prezzo)) {
                        $errors[] = "Il prezzo deve essere un numero maggiore di zero";
                    }

                    if (!$this->validateFieldStringNotNullOrEmpty($Comune)) {
                        $errors[] = "Inserire un comune";
                    }

                    if (!$this->validateFieldIntNotNullOrZero($Superficie)) {
                        $errors[] = "La superficie deve essere un numero maggiore di zero";
                    }

                    if (!$this->validateFieldIntNotNullOrZero($NumeroLocali)) {
                        $errors[] = "Il numero di locali deve essere un numero maggiore di zero";
                    }

                    if (!$this->validateFieldURL($Immagine)) {
                        $errors[] = "Il campo immagine deve contenere un URL valido";
                    } else {
                        if (!$this->validateImageURL($Immagine)) {
                            $errors[] = "URL non contiene una foto nei seguenti formati jpg, png, gif";
                        }
                    }

                    if (is_null($errors)) {
                        if ($annuncio->save()) {
                            unset($_POST);
                            $this->msg = "Annuncio modificato con successo!";
                        } else {
                            $this->msg = "Non è stato possibile modificare l'annuncio riprova!";
                        }
                    } else {
                        $this->msg = "Si sono verificati degli errori con la validazione di alcuni campi";
                    }

                }
            }

        } else {
            $this->msg = "Annuncio non trovato";
            $this->setHeaders(['Location' => '/404'], 404);
        }

        $this->setView('', 'dettaglio_edit');
        $this->setVariable('annuncio', $annuncio);
        $this->setVariable('msg', $this->msg);
        $this->setVariable('errors', $errors);
        $this->setVariable('isAuth', $isAuth);
    }

    /**
     * Validate Field String - Not Null or Zero
     *
     * @param $value
     * @return false|int
     */
    private function validateFieldIntNotNullOrZero($value)
    {
        return preg_match("#^[1-9][0-9]*$#", $value, $matches);
    }

    /**
     * Validate Field URL
     *
     * @param $value
     * @return mixed
     */
    private function validateFieldURL($value)
    {
        return filter_var($value, FILTER_VALIDATE_URL);
    }

    /**
     * Validate Image URL (only image with this ext .jpg .gif .png)
     *
     * @param $value
     * @return false|int
     */
    private function validateImageURL($value)
    {
        return preg_match("#(http(s?):)([/|.|\w|\s|-])*\.(?:jpg|gif|png)$#", $value, $matches);
    }

    /**
     * Validate Field Email
     *
     * @param $value
     * @return mixed
     */
    private function validateFieldEmail($value)
    {
        return filter_var($value, FILTER_VALIDATE_EMAIL);
    }

    /**
     * Validate Field String - Not Null or Empty
     *
     * @param $value
     * @return false|int
     */
    private function validateFieldStringNotNullOrEmpty($value)
    {
        return preg_match("#^[a-zA-Z0-9' ]+$#", $value, $matches);
    }

    /**
     * Validate Field Phone
     *
     * @param $value
     * @return false|int
     */
    private function validateFieldPhone($value)
    {
        return preg_match("/[0-9]{1,3}\.[0-9]{4,14}(?:x.+)?$/", $value, $matches);
    }

    /**
     * Check if URL exists
     *
     * @param $url
     * @return bool
     */
    private function existsURL($url)
    {
        if(@file_get_contents($url,false,null,0,1)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Add Dettaglio
     *
     * @param $idImmobile idImmobile
     *
     * @return void
     */
    public function addDettaglio()
    {
        $isAuth = $this->isAuth();
        if ($isAuth) {
            $this->setHeaders(['Location' => '/login'], 302);
        }

        $errors = null;
        $annuncio = new Annunci();
        if ($_SERVER['REQUEST_METHOD'] === "POST") {
            if (!array_key_exists('csrf_token', $_POST)) {
                $this->msg = "Error csrf-token";
            } else if ($_POST['csrf_token'] !== $this->session->get('csrf_token')) {
                $this->msg = "Invalid csrf-token";
            } else {
                $Categoria = trim(filter_input(INPUT_POST, 'Categoria', FILTER_SANITIZE_STRING));
                $Tipologia = trim(filter_input(INPUT_POST, 'Tipologia', FILTER_SANITIZE_STRING));
                $Contratto = trim(filter_input(INPUT_POST, 'Contratto', FILTER_SANITIZE_STRING));
                $Prezzo = trim(filter_input(INPUT_POST, 'Prezzo', FILTER_SANITIZE_NUMBER_INT));
                $Comune = trim(filter_input(INPUT_POST, 'Comune', FILTER_SANITIZE_STRING));
                $Superficie = trim(filter_input(INPUT_POST, 'Superficie', FILTER_SANITIZE_NUMBER_INT));
                $NumeroLocali = trim(filter_input(INPUT_POST, 'NumeroLocali', FILTER_SANITIZE_NUMBER_INT));
                $Immagine = trim(filter_input(INPUT_POST, 'Immagine', FILTER_SANITIZE_STRING));

                $annuncio->idAgenzia = $this->session->get('session_agenzia')->idAgenzia;
                $annuncio->Categoria = $Categoria;
                $annuncio->Tipologia = $Tipologia;
                $annuncio->Contratto = $Contratto;
                $annuncio->Prezzo = $Prezzo;
                $annuncio->Comune = $Comune;
                $annuncio->Superficie = $Superficie;
                $annuncio->NumeroLocali = $NumeroLocali;
                $annuncio->Immagine = $Immagine;

                if (!$this->validateFieldStringNotNullOrEmpty($Categoria)) {
                    $errors[] = "Inserire una categoria";
                }

                if (!$this->validateFieldStringNotNullOrEmpty($Tipologia)) {
                    $errors[] = "Inserire una tipologia";
                }

                if (!$this->validateFieldStringNotNullOrEmpty($Contratto)) {
                    $errors[] = "Inserire una contratto";
                }

                if (!$this->validateFieldIntNotNullOrZero($Prezzo)) {
                    $errors[] = "Il prezzo deve essere un numero maggiore di zero";
                }

                if (!$this->validateFieldStringNotNullOrEmpty($Comune)) {
                    $errors[] = "Inserire un comune";
                }

                if (!$this->validateFieldIntNotNullOrZero($Superficie)) {
                    $errors[] = "La superficie deve essere un numero maggiore di zero";
                }

                if (!$this->validateFieldIntNotNullOrZero($NumeroLocali)) {
                    $errors[] = "Il numero di locali deve essere un numero maggiore di zero";
                }

                if (!$this->validateFieldURL($Immagine)) {
                    $errors[] = "Il campo immagine deve contenere un URL valido";
                } else {
                    if (!$this->validateImageURL($Immagine)) {
                        $errors[] = "URL non contiene una foto nei seguenti formati jpg, png, gif";
                    }
                    if (!$this->existsURL($Immagine)) {
                        $errors[] = "URL della foto non esiste, prova un altro URL.";
                    }
                }

                if (is_null($errors)) {
                    if ($annuncio->save()) {
                        $annuncio = new Annunci();
                        unset($_POST);
                        $this->msg = "Annuncio salvato con successo!";
                    } else {
                        $this->msg = "Non è stato possibile salvare l'annuncio riprova!";
                    }
                } else {
                    $this->msg = "Si sono verificati degli errori con la validazione di alcuni campi";
                }

            }
        }
        $this->setView('', 'dettaglio_add');
        $this->setVariable('annuncio', $annuncio);
        $this->setVariable('msg', $this->msg);
        $this->setVariable('errors', $errors);
        $this->setVariable('isAuth', $isAuth);
    }

    /**
     * Delete Dettaglio
     *
     * @param $idImmobile
     */
    public function deleteDettaglio($idImmobile)
    {
        $isAuth = $this->isAuth();
        if ($isAuth) {
            $this->setHeaders(['Location' => '/login'], 302);
        }

        $errors = null;
        if ($idImmobile) {
            $annuncio = Annunci::get(['idAnnuncio' => $idImmobile, 'idAgenzia' => $this->session->get('session_agenzia')->idAgenzia]);
            if ($_SERVER['REQUEST_METHOD'] === "POST" && $annuncio) {
                if (!array_key_exists('csrf_token', $_POST)) {
                    $this->msg = "Error csrf-token";
                } else if ($_POST['csrf_token'] !== $this->session->get('csrf_token')) {
                    $this->msg = "Invalid csrf-token";
                } else {
                    if ($annuncio->delete()) {
                        unset($_POST);
                        $this->msg = "Annuncio salvato";
                    } else {
                        $this->msg = "Annuncio non salvato";
                    }
                }
            }

        } else {
            $this->msg = "Annuncio non trovato";
        }
        $this->setHeaders(['Location' => '/'], 302);
    }

    /**
     *
     * Add Contatto
     *
     */
    public function addContatto()
    {
        $errors = null;
        $contatto = new Contatti();
        $isAuth = $this->isAuth();
        $this->setVariable('isAuth', $isAuth);

        if ($_SERVER['REQUEST_METHOD'] === "POST" ) {
            if (!array_key_exists('csrf_token', $_POST)) {
                $this->msg = "Error csrf-token";
            } else if ($_POST['csrf_token'] !== $this->session->get('csrf_token')) {
                $this->msg = "Invalid csrf-token";
            } else {
                $Nome = trim(filter_input(INPUT_POST, 'Nome', FILTER_SANITIZE_STRING));
                $Cognome = trim(filter_input(INPUT_POST, 'Cognome', FILTER_SANITIZE_STRING));
                $Email = trim(filter_input(INPUT_POST, 'Email', FILTER_SANITIZE_EMAIL));
                $Telefono = trim(filter_input(INPUT_POST, 'NumeroDiTelefono', FILTER_SANITIZE_STRING));
                $Note = trim(filter_input(INPUT_POST, 'Note', FILTER_SANITIZE_STRING));
                $Privacy = trim(filter_input(INPUT_POST, 'Privacy', FILTER_SANITIZE_NUMBER_INT));
                $idAgenzia = trim(filter_input(INPUT_POST, 'idAnnuncio', FILTER_SANITIZE_NUMBER_INT));
                $idAnnuncio = trim(filter_input(INPUT_POST, 'idAgenzia', FILTER_SANITIZE_NUMBER_INT));

                $contatto->Nome = $Nome;
                $contatto->Cognome = $Cognome;
                $contatto->Email = $Email;
                $contatto->NumeroDiTelefono = $Telefono;
                $contatto->Note = $Note;
                $contatto->Privacy = $Privacy;

                if (!$this->validateFieldStringNotNullOrEmpty($Nome)) {
                    $errors[] = "Inserire un nome";
                }

                if (!$this->validateFieldStringNotNullOrEmpty($Cognome)) {
                    $errors[] = "Inserire un cognome";
                }

                if (!$this->validateFieldEmail($Email)) {
                    $errors[] = "Inserire una email valida";
                }

                if (!$this->validateFieldPhone($Telefono)) {
                    $errors[] = "Inserire una indirizzo telefonico valido";
                }

                if (!$this->validateFieldStringNotNullOrEmpty($Note)) {
                    $errors[] = "Inserire delle note";
                }

                if (!$Privacy) {
                    $errors[] = "Campo privacy obbligatorio";
                }

                if (is_null($errors)) {
                    if ($contatto->save()) {
                        unset($_POST);
                        $this->msg = "Contatto salvato con successo!";
                        $this->setHeaders(['Location' => '/dettaglio/' . $idAgenzia .'/' . $idAnnuncio], 302);
                    } else {
                        $this->msg = "Non è stato possibile salvare il contatto riprova!";
                    }
                } else {
                    $this->msg = "Si sono verificati degli errori con la validazione di alcuni campi del contatto";
                }
            }
        }

        $parse_url = array_key_exists('HTTP_REFERER', $_SERVER) ? parse_url($_SERVER['HTTP_REFERER']) : null;
        if ($parse_url && preg_match("#^/dettaglio/([0-9]+)/([0-9]+)$#", $parse_url['path'], $matches)) {
            $this->setView('', 'contatto_add');
            $annuncio = Annunci::get(['idAgenzia' => $matches[1], 'idAnnuncio' => $matches[2]]);
            $agenzia = Agenzie::get(['idAgenzia' => $matches[1]]);
            $this->setVariable('annuncio', $annuncio);
            $this->setVariable('agenzia', $agenzia);
            $this->setVariable('msg', $this->msg);
            $this->setVariable('errors', $errors);
            $this->setVariable('contatto', $contatto);
            $this->setVariable('isAuth', $isAuth);
        } else {
            $idAgenzia = trim(filter_input(INPUT_POST, 'idAnnuncio', FILTER_SANITIZE_NUMBER_INT));
            $idAnnuncio = trim(filter_input(INPUT_POST, 'idAgenzia', FILTER_SANITIZE_NUMBER_INT));
            $annuncio = Annunci::get(['idAgenzia' => $idAgenzia, 'idAnnuncio' => $idAnnuncio]);
            $agenzia = Agenzie::get(['idAgenzia' => $idAgenzia]);

            if (!is_null($agenzia) && !is_null($annuncio)) {
                $this->setView('', 'contatto_add');
                $this->setVariable('annuncio', $annuncio);
                $this->setVariable('agenzia', $agenzia);
                $this->setVariable('msg', $this->msg);
                $this->setVariable('errors', $errors);
                $this->setVariable('contatto', $contatto);
                $this->setVariable('isAuth', $isAuth);
            } else {
                $this->setHeaders(['Location' => '/404'], 404);
            }
        }
    }

    /**
     * Login
     *
     * @return void
     */
    public function login()
    {
        $isAuth = $this->isAuth();
        if (!$isAuth) {
            $this->setHeaders(['Location' => '/'], 302);
        }

        $errors = null;
        if ($_SERVER['REQUEST_METHOD'] === "POST") {
            if (!$this->session->get('csrf_token')) {
                $this->msg = "Error csrf-token";
            } else if ($_POST['csrf_token'] !== $this->session->get('csrf_token')) {
                $this->msg = "Invalid csrf-token";
            } else {
                $Email = trim(filter_input(INPUT_POST, 'Email', FILTER_SANITIZE_EMAIL));
                $Password = trim(filter_input(INPUT_POST, 'Password', FILTER_SANITIZE_STRING));

                if (!$this->validateFieldEmail($Email)) {
                    $errors[] = "Inserire una email valida";
                }

                $Agenzia = Agenzie::get(['Email' => $Email, 'Password' => md5($Password)]);
                if (!$Agenzia) {
                    $this->msg = 'Credenziali utente errate';
                    $errors[] = "Email o password errate";
                } else {
                    unset($Agenzia->Password);
                    $this->session->put('session_id', session_id());
                    $this->session->put('session_agenzia', $Agenzia);
                    $this->msg = "Login avvenuto con successo!";
                    $this->setHeaders(['Location' => '/'], 302);
                }
            }
        }

        $this->setView('', 'login');
        $this->setVariable('msg', $this->msg);
        $this->setVariable('errors', $errors);
        $this->setVariable('isAuth', $isAuth);
    }

    /**
     *  Error Page
     */
    public function getError()
    {
        $isAuth = $this->isAuth();
        $errors = null;
        $this->msg = "La pagina richiesta non è presente sul server!";
        $this->setView('', '404');
        $this->setVariable('msg', $this->msg);
        $this->setVariable('errors', $errors);
        $this->setVariable('isAuth', $isAuth);
    }

    /**
     *  Logout
     */
    public function logout()
    {
        session_destroy();
        session_unset();
        $this->setHeaders(['Location' => '/login'], 302);
    }

    /**
     * Destruct
     *
     * @throws \Exception
     */
    public function __destruct(){
        $this->render();
    }

}