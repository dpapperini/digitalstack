<?php
/**
 * Annunci class file
 *
 * PHP Version 7
 *
 * @category Model
 * @package  Digitalstack\model
 * @author   Daniele Papperini <daniele.papperini@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://smartimmo.com/
 */

namespace Digitalstack\model;

use PDO;

/**
 * Class Annunci
 *
 * @category Model
 * @package  Digitalstack\model
 * @author   Daniele Papperini <daniele.papperini@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://smartimmo.com/
 */
class Annunci extends Model
{

    /**
     * Table Name
     *
     * @var string
     */
    protected static $tableName = 'annunci';

    /**
     * Primary Key
     *
     * @var string
     */
    protected static $primaryKey = 'idAnnuncio';

    /**
     * Foreign Key
     *
     * @var string
     */
    protected static $foreignKey = 'idAgenzia';

    /**
     * Annunci constructor.
     *
     * @param bool $data
     */
    public function __construct($data = false) {
        $schema = array(
            'idAgenzia' => PDO::PARAM_INT,
            'idAnnuncio' => PDO::PARAM_INT,
            'Categoria' => PDO::PARAM_STR,
            'Tipologia' => PDO::PARAM_STR,
            'Contratto' => PDO::PARAM_STR,
            'Prezzo' => PDO::PARAM_INT,
            'Comune' => PDO::PARAM_STR,
            'Superficie' => PDO::PARAM_INT,
            'NumeroLocali' => PDO::PARAM_INT,
            'Immagine' => PDO::PARAM_STR
        );
        parent::__construct($schema, $data);
    }

    /**
     * Save Annuncio
     *
     * @return bool
     */
    public function save()
    {
        if ($this->idAnnuncio !== null) {
            return parent::updateAnnunci();
        }
        return parent::insertAnnunci();
    }

    /**
     * Delete Annuncio
     *
     * @return bool
     */
    public function delete()
    {
        if ($this->idAnnuncio !== null && $this->idAgenzia !== null) {
            return parent::deleteAnnunci();
        } else {
            return false;
        }
    }

}