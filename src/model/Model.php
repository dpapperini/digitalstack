<?php
/**
 * Model class file
 *
 * PHP Version 7
 *
 * @category Model
 * @package  Digitalstack\model
 * @author   Daniele Papperini <daniele.papperini@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://smartimmo.com/
 */

namespace Digitalstack\model;

use PDO;

/**
 * Class Model
 *
 * @category Model
 * @package  Digitalstack\model
 * @author   Daniele Papperini <daniele.papperini@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://smartimmo.com/
 */
abstract class Model {

    /**
     * PDO Connection
     *
     * @var
     */
    private static $pdo;

    /**
     * Table Name
     *
     * @var string
     */
    protected static $tableName = '';

    /**
     * Primary Key
     *
     * @var string
     */
    protected static $primaryKey = '';

    /**
     * Foreign Key
     *
     * @var string
     */
    protected static $foreignKey = '';

    /**
     * Fields
     *
     * @var array
     */
    private $fields = [];

    /**
     * Model constructor.
     *
     * @param $schema
     * @param bool $data
     */
    public function __construct($schema, $data = false)
    {
        $this->fields[static::$primaryKey] = array('value' => null, 'type' => PDO::PARAM_INT);
        foreach ($schema as $name => $type) {
            $this->fields[$name] = array('value' => null, 'type' => $type);
        }
        if ($data) {
            foreach ($data as $column => $value) {
                $prop = self::getPropertyName($column);
                $this->fields[$prop]['value'] = $value;
            }
        }
    }

    /**
     * Return PDO Object
     *
     * @return PDO
     */
    protected static function getPDO()
    {
        if (!isset(self::$pdo)) {
            self::$pdo = new PDO('sqlite:'.PATH_TO_SQLITE_FILE);
            self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        return self::$pdo;
    }

    /**
     * Get Class Name Call Model
     *
     * @return string
     */
    protected static function getClassName()
    {
        return strtolower(get_called_class());
    }

    /**
     * Get Model Name
     *
     * @return string
     */
    protected static function getModelName()
    {
        return strtolower(substr(get_called_class(), strrpos(get_called_class(), '\\') + 1));
    }

    /**
     * Get Table Name
     *
     * @return string
     */
    protected static function getTableName()
    {
        return self::getModelName();
    }

    /**
     * Get Field Name
     *
     * @param $field
     * @return mixed
     */
    protected static function getFieldName($field)
    {
        return $field;
    }

    /**
     * Get Bind Name
     *
     * @param $field
     * @return string
     */
    protected static function getBindName($field)
    {
        return ":{$field}";
    }

    /**
     * Get Property Name
     *
     * @param $prop
     * @return mixed
     */
    protected static function getPropertyName($prop)
    {
        return $prop;
    }

    /**
     * Get All Data By Condition Order Index anc Count
     *
     * Example: getAllBy(['idAgenzia' => 1, 'idAnnuncio' => 1], 'idAgenzia ASC', 0, 25)
     *
     * @param array $condition
     * @param string $order
     * @param null $startIndex
     * @param null $count
     * @return array|null
     */
    protected static function getAllBy($condition = [], $order = '', $startIndex = null, $count = null) {
        $query = self::setSimpleQuery($condition, $order, $startIndex, $count);
        $db = self::getPDO()->prepare($query);
        if(!empty($condition)){
            foreach ($condition as $key => $value) {
                $bindName = self::getBindName($key);
                $db->bindValue($bindName, $value);
            }
        }
        $db->execute();
        $data = $db->fetchAll(PDO::FETCH_ASSOC);
        if ($data) {
            $models = array();
            foreach ($data as $d) {
                $modelName = self::getClassName();
                $models[] = new $modelName($d);
            }
            return $models;
        }
        return null;
    }

    /**
     * Get One Object Data By Condition Order Index anc Count
     *
     * Example: getBy(['idAgenzia' => 1, 'idAnnuncio' => 1], 'idAgenzia ASC', 0, 25)
     *
     * @param array $condition
     * @param string $order
     * @param null $startIndex
     * @param null $count
     * @return null
     */
    protected static function getBy($condition = [], $order = '', $startIndex = null, $count = null) {
        $query = self::setSimpleQuery($condition, $order, $startIndex, $count);
        $db = self::getPDO()->prepare($query);
        if(!empty($condition)){
            foreach ($condition as $key => $value) {
                $bindName = self::getBindName($key);
                $db->bindValue($bindName, $value);
            }
        }

        $db->execute();
        $data = $db->fetch(PDO::FETCH_ASSOC);
        if ($data) {
            $modelName = self::getClassName();
            return new $modelName($data);
        }
        return null;
    }

    /**
     * Get All Data By Agenzie and Annunci
     *
     * @return array|null
     */
    protected static function getByAnnunciAndAgenzie() {
        $query = "SELECT annunci.*, agenzie.RagioneSociale FROM annunci INNER JOIN agenzie ON agenzie.idAgenzia = annunci.idAgenzia";
        $db = self::getPDO()->prepare($query);
        $db->execute();
        $data = $db->fetchAll(PDO::FETCH_ASSOC);
        if ($data) {
            return $data;
        }
        return null;
    }

    /**
     * Get New Id Annuncio Before Insert
     *
     * @param array $condition
     * @return int
     */
    public static function getNewIdAnnuncio($condition = [])
    {
        $newIdAnnuncio = 1;
        if ($condition) {
            $query = "SELECT MAX(idAnnuncio)+1 AS newIdAnnuncio FROM annunci";
            if(!empty($condition)){
                $query .= " WHERE ";
                foreach ($condition as $key => $value) {
                    $fieldName = self::getFieldName($key);
                    $bindName = self::getBindName($key);
                    $query .= "{$fieldName} = {$bindName} AND ";
                }
            }
            $query = rtrim($query,' AND ');
            $db = Model::getPDO()->prepare($query);
            if(!empty($condition)){
                foreach ($condition as $key => $value) {
                    $bindName = self::getBindName($key);
                    $db->bindValue($bindName, $value);
                }
            }
            $db->execute();
            $data = $db->fetch(PDO::FETCH_ASSOC);
            if ($data) {
                $newIdAnnuncio = $data['newIdAnnuncio'];
            }
        }
        return $newIdAnnuncio;
    }

    /**
     * Get One Object Data By Condition Order Index anc Count
     *
     * Example: get(['idAgenzia' => 1, 'idAnnuncio' => 1], 'idAgenzia ASC', 0, 25)
     *
     * @param array $condition
     * @param string $order
     * @param null $startIndex
     * @param null $count
     * @return null
     */
    public static function get($condition = [], $order = '', $startIndex = null, $count = null)
    {
        return self::getBy($condition, $order, $startIndex, $count);
    }

    /**
     * GetAll Data With Multiple Condition (only AND operator for the moment)
     *
     * Example: getAll(['idAgenzia' => 1, 'idAnnuncio' => 1], 'idAgenzia ASC', 0, 25)
     *
     * @param array $condition
     * @param string $order
     * @param null $startIndex
     * @param null $count
     * @return array|null
     */
    public static function getAll($condition = [], $order = '', $startIndex = null, $count = null)
    {
        return self::getAllBy($condition, $order, $startIndex, $count);
    }

    /**
     * Get Annunci and Agenzie
     *
     * @return array|null
     */
    public static function getAnnunciAndAgenzie()
    {
        return self::getByAnnunciAndAgenzie();
    }

    /**
     * Set Simple Query
     *
     * Example: setSimpleQuery(['idAgenzia' => 1, 'idAnnuncio' => 1], 'idAgenzia ASC', 0, 25)
     *
     * @param array $condition
     * @param string $order
     * @param null $startIndex
     * @param null $count
     * @return string
     */
    private static function setSimpleQuery($condition = [], $order = '', $startIndex = null, $count = null)
    {
        $tableName = self::getTableName();
        $q = "SELECT * FROM {$tableName}";
        if(!empty($condition)){
            $q .= " WHERE ";
            foreach ($condition as $key => $value) {
                $fieldName = self::getFieldName($key);
                $bindName = self::getBindName($key);
                $q .= "{$fieldName} = {$bindName} AND ";
            }
        }
        $q = rtrim($q,' AND ');
        if($order){
            $q .= " ORDER BY " . $order;
        }
        if($startIndex !== NULL){
            $q .= " LIMIT " . $startIndex;
            if($count){
                $q .= "," . $count;
            }
        }
        return $q;
    }

    /**
     * Delete For All Other Model Class
     *
     * @return bool
     */
    public function delete()
    {
        $tableName = self::getTableName();
        $fieldName = self::getFieldName(static::$primaryKey);
        $bindName = self::getBindName(static::$primaryKey);
        $q = "DELETE FROM {$tableName} WHERE {$fieldName} = {$bindName}";

        $db = Model::getPDO()->prepare($q);
        foreach ($this->fields as $field => $f) {
            $db->bindValue(self::getBindName($field), $f['value'], $f['type']);
        }
        return $db->execute();
    }

    /**
     * Simple Save For All Other Model Class
     *
     * @return bool
     */
    public function save()
    {
        $tableName = self::getTableName();
        if ($this->fields[static::$primaryKey]['value'] != null) {
            foreach ($this->fields as $field => $f) {
                if ($field !== static::$primaryKey && $f['value'] !== null) {
                    $fieldName = self::getFieldName($field);
                    $bindName = self::getBindName($field);
                    $fields[] = "{$fieldName} = {$bindName}";
                }
            }
            $fieldName = self::getFieldName(static::$primaryKey);
            $bindName = self::getBindName(static::$primaryKey);
            $set = implode(', ', $fields);
            $q = "UPDATE {$tableName} SET {$set} WHERE {$fieldName} = {$bindName}";
        } else {
            foreach ($this->fields as $field => $f) {
                if ($field !== static::$primaryKey && $f['value'] !== null) {
                    $cols[] = self::getFieldName($field);
                    $binds[] = self::getBindName($field);
                }
            }
            $columns = implode(', ', $cols);
            $bindings = implode(', ', $binds);
            $q = "INSERT INTO {$tableName} ({$columns}) VALUES ({$bindings})";
        }

        $db = Model::getPDO()->prepare($q);
        foreach ($this->fields as $field => $f) {
            if ($f['value'] !== null) {
                $db->bindValue(self::getBindName($field), $f['value'], $f['type']);
            }
        }
        return $db->execute();
    }

    /**
     * Delete Annunci
     *
     * @return bool
     */
    public function deleteAnnunci()
    {
        $tableName = self::getTableName();
        $query = "DELETE FROM {$tableName}";

        $fieldNamePrimaryKey = self::getFieldName(static::$primaryKey);
        $bindNamePrimaryKey = self::getBindName(static::$primaryKey);
        $fieldNameForeignKey = self::getFieldName(static::$foreignKey);
        $bindNameForeignKey = self::getBindName(static::$foreignKey);
        $query .= " WHERE {$fieldNamePrimaryKey} = {$bindNamePrimaryKey} AND {$fieldNameForeignKey} = {$bindNameForeignKey}";

        $db = Model::getPDO()->prepare($query);
        foreach ($this->fields as $field => $f) {
            if ($f['value'] !== null && ($field === static::$primaryKey || $field === static::$foreignKey)) {
                $db->bindParam(self::getBindName($field), $f['value'], $f['type']);
            }
        }
        return $db->execute();
    }

    /**
     * Save Annunci
     *
     * @return bool
     */
    public function insertAnnunci()
    {
        $tableName = self::getTableName();
        $this->fields[static::$primaryKey]['value'] = self::getNewIdAnnuncio([static::$foreignKey => $this->fields[static::$foreignKey]['value']]);
        foreach ($this->fields as $field => $f) {
            if ($f['value'] !== null) {
                $cols[] = self::getFieldName($field);
                $binds[] = self::getBindName($field);
            }
        }
        $columns = implode(', ', $cols);
        $bindings = implode(', ', $binds);
        $q = "INSERT INTO {$tableName} ({$columns}) VALUES ({$bindings})";
        $db = Model::getPDO()->prepare($q);
        foreach ($this->fields as $field => $f) {
            if ($f['value'] !== null) {
                $db->bindValue(self::getBindName($field), $f['value'], $f['type']);
            }
        }
        return $db->execute();
    }

    /**
     * Update Annunci
     *
     * @return bool
     */
    public function updateAnnunci()
    {
        $tableName = self::getTableName();
        foreach ($this->fields as $field => $f) {
            if ($field !== static::$primaryKey && $f['value'] !== null) {
                $fieldName = self::getFieldName($field);
                $bindName = self::getBindName($field);
                $fields[] = "{$fieldName} = {$bindName}";
            }
        }
        $set = implode(', ', $fields);
        $q = "UPDATE {$tableName} SET {$set}";

        $fieldNamePrimaryKey = self::getFieldName(static::$primaryKey);
        $bindNamePrimaryKey = self::getBindName(static::$primaryKey);
        $fieldNameForeignKey = self::getFieldName(static::$foreignKey);
        $bindNameForeignKey = self::getBindName(static::$foreignKey);
        $q .= " WHERE {$fieldNamePrimaryKey} = {$bindNamePrimaryKey} AND {$fieldNameForeignKey} = {$bindNameForeignKey}";

        $db = Model::getPDO()->prepare($q);
        foreach ($this->fields as $field => $f) {
            if ($f['value'] !== null) {
                $db->bindValue(self::getBindName($field), $f['value'], $f['type']);
            }
        }
        return $db->execute();
    }

    /**
     * Set Field
     *
     * @param $name
     * @param $value
     */
    public function __set($name, $value)
    {
        if (array_key_exists($name, $this->fields)) {
            $this->fields[$name]['value'] = $value;
        }
    }

    /**
     * Get Field
     *
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        if (array_key_exists($name, $this->fields)) {
            return $this->fields[$name]['value'];
        }
    }

}