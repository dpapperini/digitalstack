<?php
/**
 * Agenzie class file
 *
 * PHP Version 7
 *
 * @category Model
 * @package  Digitalstack\model
 * @author   Daniele Papperini <daniele.papperini@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://smartimmo.com/
 */

namespace Digitalstack\model;

use PDO;

/**
 * Class Agenzie
 *
 * @category Model
 * @package  Digitalstack\model
 * @author   Daniele Papperini <daniele.papperini@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://smartimmo.com/
 */
class Agenzie extends Model
{

    /**
     * Table Name
     *
     * @var string
     */
    protected static $tableName = 'agenzie';

    /**
     * PrimaryKey Name
     *
     * @var string
     */
    protected static $primaryKey = 'idAgenzia';

    /**
     * Agenzie constructor.
     *
     * @param bool $data
     */
    public function __construct($data = false) {
        $schema = array(
            'Email' => PDO::PARAM_STR,
            'RagioneSociale' => PDO::PARAM_STR,
            'Password' => PDO::PARAM_STR
        );
        parent::__construct($schema, $data);
    }

}