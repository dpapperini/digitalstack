<?php
/**
 * Contatti class file
 *
 * PHP Version 7
 *
 * @category Model
 * @package  Digitalstack\model
 * @author   Daniele Papperini <daniele.papperini@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://smartimmo.com/
 */
namespace Digitalstack\model;

use PDO;

/**
 * Class Contatti
 *
 * @category Model
 * @package  Digitalstack\model
 * @author   Daniele Papperini <daniele.papperini@gmail.com>
 * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
 * @link     http://smartimmo.com/
 */
class Contatti extends Model
{
    /**
     * Table Name
     *
     * @var string
     */
    protected static $tableName = 'contatti';

    /**
     * Primary Key
     *
     * @var string
     */
    protected static $primaryKey = 'idContatto';

    /**
     * Contatti constructor.
     *
     * @param bool $data
     */
    public function __construct($data = false) {
        $schema = array(
            'Nome' => PDO::PARAM_STR,
            'Cognome' => PDO::PARAM_STR,
            'Email' => PDO::PARAM_STR,
            'NumeroDiTelefono' => PDO::PARAM_STR,
            'Note' => PDO::PARAM_STR,
            'Privacy' => PDO::PARAM_BOOL
        );
        parent::__construct($schema, $data);
    }
}