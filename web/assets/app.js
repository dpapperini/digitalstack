$(function() {

    $('.btn-delete').on('click', function(event) {
        event.preventDefault();
        let form = $(this).parent().get(0);
        let formData = $(form).serialize();
        // Submit the form using AJAX.
        $.ajax({
            type: 'POST',
            url: $(form).attr('action'),
            data: formData
        })
            .done(function(response) {
                window.location.reload();
            })
            .fail(function(data) {
                alert("Ci sono stati dei problemi durante la cancellazione dell'annuncio");
            });
    });

    $('.btn-contatti').on('click', function(event) {
        event.preventDefault();
        let contactDiv = $("#contatto-add");
        let form = $(this).parent().get(0);
        let formData = $(form).serialize();
        // Submit the form using AJAX.
        $.ajax({
            type: 'POST',
            url: $(form).attr('action'),
            data: formData
        })
            .done(function(response) {
                contactDiv.html(response);
            })
            .fail(function(data) {
                alert("Ci sono stati dei problemi durante l'inserimento del contatto");
            });
    });
});