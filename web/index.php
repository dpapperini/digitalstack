<?php

use Digitalstack\ImmobileController;
use \Digitalstack\dispatcher\Dispatcher;

require_once __DIR__.'/../src/boot.php';

$controller = new ImmobileController();

ini_set('session.save_handler', 'files');
session_set_save_handler($controller->session, true);
session_save_path(SESSION_VIEWS_FOLDER);
if (!$controller->session->isValid(5)) {
    $controller->session->destroy();
}
if (!$controller->session->get('csrf_token')) {
    $controller->session->put('csrf_token', bin2hex(random_bytes(32)));
}

$app = new Dispatcher($controller);
$app->handle();
