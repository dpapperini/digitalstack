
<?php echo $msg; ?>
<div class="row">
    <?php if ($errors) { ?>
        <div class="col-sm-12 alert alert-danger" role="alert">
            <?php foreach($errors as $error): ?>
                <p><?php echo $error; ?></p>
            <?php endforeach; ?>
        </div>
    <?php } ?>
</div>

<div class="row">
    <div class="col-sm-12">
        <h3>Form Contatto</h3>

        <form action="/contatto/add" method="POST">
            <input type="hidden" name="idAgenzia" value="<?php echo $annuncio->idAgenzia; ?>">
            <input type="hidden" name="idAnnuncio" value="<?php echo $annuncio->idAnnuncio; ?>">
            <input type="hidden" name="csrf_token" value="<?php echo $_SESSION['csrf_token']; ?>">
            <div class="form-group">
                <label for="Nome">Nome</label>
                <input type="text" class="form-control" id="nome" name="Nome" value="<?php echo $contatto->Nome; ?>" placeholder="Nome" required>
            </div>
            <div class="form-group">
                <label for="Cognome">Cognome</label>
                <input type="text" class="form-control" id="Cognome" name="Cognome" value="<?php echo $contatto->Cognome; ?>" placeholder="Cognome" required>
            </div>
            <div class="form-group">
                <label for="Email">Email</label>
                <input type="text" class="form-control" id="Email" name="Email" value="<?php echo $contatto->Email; ?>" placeholder="Email" required>
            </div>
            <div class="form-group">
                <label for="NumeroDiTelefono">Telefono</label>
                <input type="text" class="form-control" id="NumeroDiTelefono" name="NumeroDiTelefono" value="<?php echo $contatto->NumeroDiTelefono; ?>" placeholder="Telefono es: 39.123456789" required>
            </div>
            <div class="form-group">
                <label for="Note">Note</label>
                <input type="text" class="form-control" id="Note" name="Note" value="<?php echo $contatto->Note; ?>" placeholder="Note" required>
            </div>
            <div class="form-group form-check">
                <input type="hidden" name="Privacy" value="0" />
                <input type="checkbox" class="form-check-input" id="Privacy" name="Privacy" value="1" required>
                <label class="form-check-label" for="Privacy">Privacy</label>
            </div>
            <button type="submit" class="btn btn-primary btn-contatti">Submit</button>
        </form>

    </div>
</div>