<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login</title>

    <link href="/assets/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/starter-template.css" rel="stylesheet">

    
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">

            <a class="navbar-brand" href="/">Smart Immo</a>
            <?php if($isAuth) { ?>
                <a class="navbar-brand" href="/login">Login</a>
            <?php } else { ?>
                <a class="navbar-brand" href="/logout">Logout</a>
                <a class="navbar-brand" href="/dettaglio/add">Nuovo Annuncio</a>
            <?php } ?>

        </div>
        
      </div>
    </nav>

    
    <div class="jumbotron">
      <div class="container">

          <div class="row">
              <div class="col-md-12">
                  <h1>Effettua il login</h1>
                  <p>Per visualizzare i tuoi annunci.</p>
              </div>
          </div>


          <?php echo $msg; ?>
          <div class="row">
              <?php if ($errors) { ?>
                  <div class="col-sm-12 alert alert-danger" role="alert">
                      <?php foreach($errors as $error): ?>
                          <p><?php echo $error; ?></p>
                      <?php endforeach; ?>
                  </div>
              <?php } ?>
          </div>

      </div>
    </div>

    <div class="container">
      
      <div class="row">
        <div class="col-md-12">
            <h3>Login</h3>
            <form method="POST" action="/login">
                <div class="form-group">
                    <label for="Email">Email address</label>
                    <input type="email" class="form-control" id="Email" name="Email" value="<?php echo isset($_POST['Email']) ? $_POST['Email'] : ""; ?>" required>
                </div>
                <div class="form-group">
                    <label for="Password">Password</label>
                    <input type="password" class="form-control" id="Password" name="Password" value="<?php echo isset($_POST['Password']) ? $_POST['Password'] : ""; ?>" required>
                </div>
                <button type="submit" class="btn btn-primary" name="csrf_token" value="<?php echo $_SESSION['csrf_token']; ?>">Submit</button>
            </form>
        </div>
      </div>

      <hr>

      <footer>
        <p>&copy; 2020 SmartImmo</p>
      </footer>
    </div>


    <script src="/assets/jquery.min.js"></script>
    <script src="/assets/bootstrap.min.js"></script>
  </body>
</html>