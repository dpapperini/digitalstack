<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Home Page</title>

    <link href="/assets/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/starter-template.css" rel="stylesheet">

    
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">

            <a class="navbar-brand" href="/">Smart Immo</a>
            <?php if($isAuth) { ?>
                <a class="navbar-brand" href="/login">Login</a>
            <?php } else { ?>
                <a class="navbar-brand" href="/logout">Logout</a>
                <a class="navbar-brand" href="/dettaglio/add">Nuovo Annuncio</a>
            <?php } ?>
        </div>

      </div>
    </nav>


    <div class="jumbotron">
      <div class="container">
          <?php echo isset($msg) ? $msg : ""; ?>
          <?php if (!$isAuth) { ?>

              <div class="row">
                  <div class="col-md-12">
                      <h1>Benvenuto nel tuo gestionale!</h1>
                      <p><?php echo isset($agenzia_annunci) ? "Ci sono " . count($agenzia_annunci) . " creati da te." : "Non ci sono annunci. Creane uno nuovo."; ?></p>
                      <p><a class="btn btn-sm btn-outline-secondary" href="/dettaglio/add">Nuovo Annuncio</a></p>
                  </div>
              </div>

          <?php } else { ?>

              <div class="row">
                  <div class="col-md-12">
                      <h1>Effettua il login</h1>
                      <p>Per visualizzare i tuoi annunci.</p>
                  </div>
              </div>

          <?php } ?>

          <?php if (!$isAuth && isset($agenzia_annunci)) { ?>
              <div class="row">
                  <div class="col-md-12">
                      <h1>Tutti i tuoi annunci</h1>
                  </div>
              </div>
              <div class="row">
          <?php foreach ($agenzia_annunci as $annuncio):

              $image = $this->getDataURI($annuncio->Immagine);

              ?>
              <div class="col-md-4">
                  <div class="card mb-4 box-shadow">
                      <img class="card-img-top" src="<?php echo $image; ?>" style="width: 100%;" />
                      <div class="card-body">
                          <dl class="row">
                              <dt class="col-sm-6">Agenzia</dt>
                              <dd class="col-sm-6"><?php echo $agenzia->RagioneSociale; ?></dd>

                              <dt class="col-sm-6">Annuncio</dt>
                              <dd class="col-sm-6"><?php echo $annuncio->idAnnuncio; ?></dd>

                              <dt class="col-sm-6">Categoria</dt>
                              <dd class="col-sm-6"><?php echo $annuncio->Categoria; ?></dd>

                              <dt class="col-sm-6">Tipologia</dt>
                              <dd class="col-sm-6"><?php echo $annuncio->Tipologia; ?></dd>

                              <dt class="col-sm-6">Contratto</dt>
                              <dd class="col-sm-6"><?php echo $annuncio->Contratto; ?></dd>

                              <dt class="col-sm-6">Prezzo</dt>
                              <dd class="col-sm-6"><?php echo $annuncio->Prezzo; ?></dd>

                              <dt class="col-sm-6">Comune</dt>
                              <dd class="col-sm-6"><?php echo $annuncio->Comune; ?></dd>

                              <dt class="col-sm-6">Superficie</dt>
                              <dd class="col-sm-6"><?php echo $annuncio->Superficie; ?></dd>

                              <dt class="col-sm-6">Numero Locali</dt>
                              <dd class="col-sm-6"><?php echo $annuncio->NumeroLocali; ?></dd>
                          </dl>
                          <div class="d-flex justify-content-between align-items-center">
                              <div class="btn-group">
                                  <a class="btn btn-sm btn-outline-secondary" href="/dettaglio/<?php echo $annuncio->idAgenzia; ?>/<?php echo $annuncio->idAnnuncio; ?>">View</a>
                                  <a class="btn btn-sm btn-outline-secondary" href="/dettaglio/edit/<?php echo $annuncio->idAnnuncio; ?>">Edit</a>
                              </div>
                              <form action="/dettaglio/delete/<?php echo $annuncio->idAnnuncio; ?>" method="POST">
                                  <input type="hidden" name="csrf_token" value="<?php echo $_SESSION['csrf_token']; ?>">
                                  <input type="hidden" name="idAnnuncio" value="<?php echo $annuncio->idAnnuncio; ?>">
                                  <button class="btn btn-sm btn-outline-secondary btn-delete" type="submit">Delete</button>
                              </form>
                          </div>
                      </div>
                  </div>
              </div>

          <?php endforeach; ?>
              </div>
          <?php } ?>

            </div>
        </div>
    </div>

    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <h1>Tutti gli annunci</h1>
            </div>
        </div>

        <div class="row">
            <?php

            if (isset($annunci)) {
                foreach ($annunci as $annuncio):
                    $image = $this->getDataURI($annuncio['Immagine']);
                ?>
                <div class="col-md-4">
                    <div class="card mb-4 box-shadow">
                        <img class="card-img-top" src="<?php echo $image; ?>" style="width: 100%;" />
                        <div class="card-body">
                            <dl class="row">
                                <dt class="col-sm-6">Agenzia</dt>
                                <dd class="col-sm-6"><?php echo $annuncio['RagioneSociale']; ?></dd>

                                <dt class="col-sm-6">Annuncio</dt>
                                <dd class="col-sm-6"><?php echo $annuncio['idAnnuncio']; ?></dd>

                                <dt class="col-sm-6">Categoria</dt>
                                <dd class="col-sm-6"><?php echo $annuncio['Categoria']; ?></dd>

                                <dt class="col-sm-6">Tipologia</dt>
                                <dd class="col-sm-6"><?php echo $annuncio['Tipologia']; ?></dd>

                                <dt class="col-sm-6">Contratto</dt>
                                <dd class="col-sm-6"><?php echo $annuncio['Contratto']; ?></dd>

                                <dt class="col-sm-6">Prezzo</dt>
                                <dd class="col-sm-6"><?php echo $annuncio['Prezzo']; ?></dd>

                                <dt class="col-sm-6">Comune</dt>
                                <dd class="col-sm-6"><?php echo $annuncio['Comune']; ?></dd>

                                <dt class="col-sm-6">Superficie</dt>
                                <dd class="col-sm-6"><?php echo $annuncio['Superficie']; ?></dd>

                                <dt class="col-sm-6">Numero Locali</dt>
                                <dd class="col-sm-6"><?php echo $annuncio['NumeroLocali']; ?></dd>
                            </dl>

                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <a class="btn btn-sm btn-outline-secondary" href="/dettaglio/<?php echo $annuncio['idAgenzia']; ?>/<?php echo $annuncio['idAnnuncio']; ?>">View</a>
                                    <?php if (!$isAuth && $_SESSION['session_agenzia']->idAgenzia === $annuncio['idAgenzia']) { ?>
                                    <a class="btn btn-sm btn-outline-secondary" href="/dettaglio/edit/<?php echo $annuncio['idAnnuncio']; ?>">Edit</a>
                                    <?php } ?>
                                </div>
                                <?php if (!$isAuth && $_SESSION['session_agenzia']->idAgenzia === $annuncio['idAgenzia']) { ?>
                                <form action="/dettaglio/delete/<?php echo $annuncio['idAnnuncio']; ?>" method="POST">
                                    <input type="hidden" name="csrf_token" value="<?php echo $_SESSION['csrf_token']; ?>">
                                    <input type="hidden" name="idAnnuncio" value="<?php echo $annuncio['idAnnuncio']; ?>">
                                    <button class="btn btn-sm btn-outline-secondary btn-delete" type="submit">Delete</button>
                                </form>
                                <?php } ?>
                            </div>

                        </div>
                    </div>
                </div>

            <?php

            endforeach;

            }
            ?>

        </div>

      <hr>

      <footer>
        <p>&copy; 2020 SmartImmo</p>
      </footer>
    </div>


    <script src="/assets/jquery.min.js"></script>
    <script src="/assets/bootstrap.min.js"></script>
    <script src="/assets/app.js"></script>
  </body>
</html>