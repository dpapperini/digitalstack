<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Dettaglio annuncio <?php echo $annuncio->idAnnuncio; ?></title>

    <link href="/assets/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/starter-template.css" rel="stylesheet">

    
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">

            <a class="navbar-brand" href="/">Smart Immo</a>
            <?php if($isAuth) { ?>
                <a class="navbar-brand" href="/login">Login</a>
            <?php } else { ?>
                <a class="navbar-brand" href="/logout">Logout</a>
                <a class="navbar-brand" href="/dettaglio/add">Nuovo Annuncio</a>
            <?php } ?>
        </div>
        
      </div>
    </nav>

    <div class="container">

        <?php echo $msg; ?>
        <div class="row">
            <?php if ($errors) { ?>
                <div class="col-sm-12 alert alert-danger" role="alert">
                    <?php foreach($errors as $error): ?>
                        <p><?php echo $error; ?></p>
                    <?php endforeach; ?>
                </div>
            <?php } ?>
        </div>
      
      <div class="row">

          <div class="col-md-12">
              <h1>Dettaglio annuncio <?php echo $annuncio->idAnnuncio; ?></h1>
              <div class="row">
                  <div class="col-sm-12 col-md-6">
                      <img src="<?php $image = $this->getDataURI($annuncio->Immagine); echo $image; ?>" style="width:100%" />
                  </div>
                  <div class="col-sm-12 col-md-6">
                      <dl class="row">
                          <dt class="col-sm-3">Agenzia</dt>
                          <dd class="col-sm-9"><?php echo $agenzia->RagioneSociale; ?></dd>

                          <dt class="col-sm-3">Annuncio</dt>
                          <dd class="col-sm-9"><?php echo $annuncio->idAnnuncio; ?></dd>

                          <dt class="col-sm-3">Categoria</dt>
                          <dd class="col-sm-9"><?php echo $annuncio->Categoria; ?></dd>

                          <dt class="col-sm-3">Tipologia</dt>
                          <dd class="col-sm-9"><?php echo $annuncio->Tipologia; ?></dd>

                          <dt class="col-sm-3">Contratto</dt>
                          <dd class="col-sm-9"><?php echo $annuncio->Contratto; ?></dd>

                          <dt class="col-sm-3">Prezzo</dt>
                          <dd class="col-sm-9"><?php echo $annuncio->Prezzo; ?></dd>

                          <dt class="col-sm-3">Comune</dt>
                          <dd class="col-sm-9"><?php echo $annuncio->Comune; ?></dd>

                          <dt class="col-sm-3">Superficie</dt>
                          <dd class="col-sm-9"><?php echo $annuncio->Superficie; ?></dd>

                          <dt class="col-sm-3">Numero Locali</dt>
                          <dd class="col-sm-9"><?php echo $annuncio->NumeroLocali; ?></dd>

                          <dt class="col-sm-3">Immagine</dt>
                          <dd class="col-sm-9"><?php echo $annuncio->Immagine; ?></dd>
                      </dl>
                  </div>
              </div>

              <div class="row">
                  <div class="col-sm-12" id="contatto-add">
                      <?php include "contatto_add.php"; ?>
                  </div>
              </div>

          </div>

      </div>

      <hr>

      <footer>
        <p>&copy; 2020 SmartImmo</p>
      </footer>
    </div>


    <script src="/assets/jquery.min.js"></script>
    <script src="/assets/bootstrap.min.js"></script>
    <script src="/assets/app.js"></script>
  </body>
</html>