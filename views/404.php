<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Page not found - Error 404</title>

    <link href="/assets/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/starter-template.css" rel="stylesheet">


</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">

            <a class="navbar-brand" href="/">Smart Immo</a>
            <?php if($isAuth) { ?>
                <a class="navbar-brand" href="/login">Login</a>
            <?php } else { ?>
                <a class="navbar-brand" href="/logout">Logout</a>
                <a class="navbar-brand" href="/dettaglio/add">Nuovo Annuncio</a>
            <?php } ?>

        </div>

    </div>
</nav>


<div class="jumbotron">
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <h1>Page not found - Error 404</h1>
            </div>
        </div>

    </div>
</div>

<div class="container">

    <div class="row">
        <div class="col-md-12">
            <h3><?php echo isset($msg) ? $msg : null; ?></h3>

        </div>
    </div>

    <hr>

    <footer>
        <p>&copy; 2020 SmartImmo</p>
    </footer>
</div>


<script src="/assets/jquery.min.js"></script>
<script src="/assets/bootstrap.min.js"></script>
</body>
</html>