<!DOCTYPE html>
<html lang="it">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Modifica dettaglio annuncio <?php echo $annuncio->idAnnuncio; ?></title>

    <link href="/assets/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/starter-template.css" rel="stylesheet">


</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">Smart Immo</a>
            <?php if($isAuth) { ?>
                <a class="navbar-brand" href="/login">Login</a>
            <?php } else { ?>
                <a class="navbar-brand" href="/logout">Logout</a>
                <a class="navbar-brand" href="/dettaglio/add">Nuovo Annuncio</a>
            <?php } ?>
        </div>

    </div>
</nav>

<div class="container">

    <div class="row">

        <div class="col-md-12">
            <h1>Modifica dettaglio annuncio <?php echo $annuncio->idAnnuncio; ?></h1>

            <?php echo $msg; ?>
            <div class="row">
                <?php if ($errors) { ?>
                    <div class="col-sm-12 alert alert-danger" role="alert">
                        <?php foreach($errors as $error): ?>
                            <p><?php echo $error; ?></p>
                        <?php endforeach; ?>
                    </div>
                <?php } ?>
            </div>

            <div class="row">
                <div class="col-sm-12 col-md-6">
                    <img src="<?php $image = $this->getDataURI($annuncio->Immagine); echo $image; ?>" style="width:100%" />
                </div>
                <div class="col-sm-12 col-md-6">
                    <form action="/dettaglio/edit/<?php echo $annuncio->idAnnuncio; ?>" method="POST">
                        <input type="hidden" name="idAnnuncio" value="<?php echo $annuncio->idAnnuncio; ?>">
                        <input type="hidden" name="idAgenzia" value="<?php echo $annuncio->idAgenzia; ?>">
                        <div class="form-group">
                            <label for="Categoria">Categoria</label>
                            <input type="text" class="form-control" id="Categoria" name="Categoria" value="<?php echo $annuncio->Categoria; ?>">
                        </div>
                        <div class="form-group">
                            <label for="Categoria">Tipologia</label>
                            <input type="text" class="form-control" id="Tipologia" name="Tipologia" value="<?php echo $annuncio->Tipologia; ?>">
                        </div>
                        <div class="form-group">
                            <label for="Categoria">Contratto</label>
                            <input type="text" class="form-control" id="Contratto" name="Contratto" value="<?php echo $annuncio->Contratto; ?>">
                        </div>
                        <div class="form-group">
                            <label for="Categoria">Prezzo</label>
                            <input type="number" class="form-control" id="Prezzo" name="Prezzo" value="<?php echo $annuncio->Prezzo; ?>">
                        </div>
                        <div class="form-group">
                            <label for="Categoria">Comune</label>
                            <input type="text" class="form-control" id="Prezzo" name="Comune" value="<?php echo $annuncio->Comune; ?>">
                        </div>
                        <div class="form-group">
                            <label for="Categoria">Superficie</label>
                            <input type="number" class="form-control" id="Superficie" name="Superficie" value="<?php echo $annuncio->Superficie; ?>">
                        </div>
                        <div class="form-group">
                            <label for="Categoria">Numero Locali</label>
                            <input type="number" class="form-control" id="NumeroLocali" name="NumeroLocali" value="<?php echo $annuncio->NumeroLocali; ?>">
                        </div>
                        <div class="form-group">
                            <label for="Categoria">Immagine</label>
                            <input type="text" class="form-control" id="Immagine" name="Immagine" value="<?php echo $annuncio->Immagine; ?>">
                        </div>
                        <button type="submit" class="btn btn-primary" name="csrf_token" value="<?php echo $_SESSION['csrf_token']; ?>">Submit</button>
                    </form>

                </div>
            </div>
        </div>

    </div>

    <hr>

    <footer>
        <p>&copy; 2020 SmartImmo</p>
    </footer>
</div>


<script src="/assets/jquery.min.js"></script>
<script src="/assets/bootstrap.min.js"></script>
</body>
</html>