# Digitalstack SmartImmo
Benvenuto! Prima dell'intervista, ti chiediamo di sviluppare un piccolo progetto in _PHP_ e di inviarci il lavoro svolto.

**Cosa viene fornito**
Lo scheletro di una applicazione PHP.

**Cosa ci si aspetta**
Uno zip contenente la documentazione per l'utilizzo e il progetto modificato secondo le istruzioni oppure un link a un repository pubblico.

# Installazione dello scheletro

**Prerequisiti**
[composer](https://getcomposer.org/).

Posizionarsi nella cartella del progetto e lanciare:
```sh
$ composer install
```

# Dettaglio e specifiche del progetto
**SmartImmo** è un gestionale immobiliare al quale accedono diverse agenzie. Grazie a questo strumento, le agenzie inseriscono, aggiornano e cancellano gli annunci da pubblicare.

# Parte 1
Utilizzando solo PHP 7 puro (nessun framework), creare, seguendo il paradigma MVC, il backend di SmartImmo che dovrà prevedere:

* la pagina di **login**;
* l' **elenco degli annunci** inseriti;
* il dettaglio dell'annuncio, grazie al quale modificare i dati e salvarli.

Dovrai prevedere un pulsante per l'**eliminazione** di un annuncio, la **modifica** (che porta alla pagina di dettaglio dalla quale aggiornare tutti i campi) e l'**inserimento** di un nuovo annuncio. Nota: ogni agenzia gestisce solo i propri annunci. Ogni annuncio ha un _idAgenzia_ che lo associa all'agenzia.

Dovrai mettere in atto tutte le strategie che utilizzeresti in un ambiente di produzione (validazione dei dati, sicurezza, ecc.).

# Parte 2
Creare il frontend di un ipotetico portale con due semplici pagine:

* l'**elenco di tutti gli annunci** presenti nel database;
* una **pagina di dettaglio** per ogni annuncio, la quale dovrà prevedere una form di contatto per manifestare interesse per l'annuncio.
* Tale modulo dovrà prevedere i campi _nome_ , _cognome_ , _email_ , _numero di telefono_ , _note_ e _check per accettazionne della privacy_. Dovrai creare nel database la tabella _contatti_ e salvare i dati in questa tabella. Per crearla, puoi utilizzare _phinx_ (un tool per creare database migrations: [http://docs.phinx.org/](http://docs.phinx.org/)), che è già disponibile nel progetto, oppure scrivere la query che crea la tabella.

# Informazioni
Il database _smart.db_ è nella cartella _data_. Si tratta di un file _sqlite_ in cui sono definite e popolate due tabelle: _agenzie_ e _annunci_. La colonna password contiene gli hash in MD5 delle password: "agenzia1" e "agenzia2".

Non sarà tenuto conto in alcun modo di come verrà sviluppata l'interfaccia grafica; tuttavia nei due template inseriti nella cartella _views_ è già incluso bootstrap, ed è preferibile si usi tale libreria per le viste.

Usiamo il webserver integrato di php, che puoi lanciare in questo modo, dal folder del progetto:
```sh
$ composer run server
```

In questo modo, il progetto sarà accessibile dal tuo browser all'indirizzo [http://localhost:7000](http://localhost:7000).

I test (da inserire nella cartella tests ) possono essere lanciati con:
```sh
$ composer run test
```

# Punti di valutazione

* completezza del progetto;
* valutazione tecnica sulle scelte effettuate;
* qualità del codice;
* correttezza delle query SQL;
* completezza dei commenti e della documentazione che allegherai, giustificando le scelte effettuate nel progetto; completezza dei test scritti.

Invia tutto al tuo contatto di Digitalstack.

Grazie!
### Digitalstack